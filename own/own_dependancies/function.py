def CheckRequirement(Data:list, requirement:list, variable_requirement:list):
    if CheckDict(Data, requirement):
        validate = True
        for variable in variable_requirement:
            if validate == True:
                validation = False
                for require in variable:
                    if require in Data.keys():
                        validation = True
            if validation == False:
                validate = False
        return validate
    return False

def ReturnOptionalRequirementAvailable(Data:list, variable_requirement:list):
    keys = []
    for variable in variable_requirement:
        for require in variable:
            key = []
            if require in Data.keys():
                key.append(require)
            keys.append(key)
    return keys

def CheckDict(Dict:dict, keys:dict):
    for key in keys:
        if key in Dict.keys():
            pass
        else:
            return False
    return True